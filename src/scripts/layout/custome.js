// import $ from 'jquery';
import slick from 'slick-carousel';
export default () => {
    ((custom, $) => {
        const $dom = {};

        const cacheDom = () => {
            $dom.bodyElement = $('body');
        };
        const bindUIActions = () => {
            $(".toggle-btn").click(function () {
                $(".mobile-menu").css({
                    left: "0%"
                });
            });
            $(".close-btn").click(function () {
                $(".mobile-menu").css({
                    left: "-100%"
                });
            });

            $(document).ready(function () {
                jQuery(function () {
                    jQuery('.swatch :radio').click(function () {
                        var optionIndex = jQuery(this).closest('.swatch').attr('data-option-index');
                        var optionValue = jQuery(this).val();
                        console.log("hi");
                        jQuery(this).closest('form').find('.single-option-selector').eq(optionIndex).val(optionValue).trigger('change');
                    });
                });
            });



            //form login
            $("#forget").click(function () {
                $(".lgn").hide();
                $("#recover").show();
            });
            $(".cnl").click(function () {
                $("#recover").hide();
                $(".lgn").show();
            });

            //PDP Qty Selector
            $("button.qty-btns").on("click", function (event) {
                event.preventDefault();
                var container = $(event.currentTarget).closest('[data-qtyContainer]');
                var qtEle = $(container).find('[data-qty]');
                var currentQty = $(qtEle).val();
                var qtyDirection = $(this).data("direction");
                var newQty = 0;

                if (qtyDirection == "1") {
                    newQty = parseInt(currentQty) + 1;
                } else if (qtyDirection == "-1") {
                    newQty = parseInt(currentQty) - 1;
                }

                if (newQty == 1) {
                    $(".decrement-quantity").attr("disabled", "disabled");
                }
                if (newQty > 1) {
                    $(".decrement-quantity").removeAttr("disabled");
                }

                if (newQty > 0) {
                    newQty = newQty.toString();
                    $(qtEle).val(newQty);
                } else {
                    $(qtEle).val("1");
                }
            });

            //   var selectCallback = function(variant, selector){

            //     if (variant) {
            //     var form = jQuery('#' + selector.domIdPrefix).closest('form');
            //     for (var i=0,length=variant.options.length; i<length; i++) {
            //       var radioButton = form.find('.swatch[data-option-index="' + i + '"] :radio[value="' + variant.options[i] +'"]');
            //       if (radioButton.size()) {
            //         radioButton.get(0).checked = true;
            //       }
            //     }
            //   }
            // };

            jQuery(function () {
                jQuery('.swatch :radio').change(function () {
                    var optionIndex = jQuery(this).closest('.swatch').attr('data-option-index');
                    var optionValue = jQuery(this).val();
                    jQuery(this)
                        .closest('form')
                        .find('.single-option-selector')
                        .eq(optionIndex)
                        .val(optionValue)
                        .trigger('change');
                });
            });
            $(document).ready(function () {
                $("#quickby").click(function () {
                    addtocart = $('#AddToCartForm');
                    jQuery.ajax({
                        type: 'POST',
                        url: '/cart/add.js',
                        data: jQuery(addtocart).serialize(),
                        dataType: 'json',
                        success: function (cart) {
                            $.ajax({
                                url: '/cart',
                                success: function success(data) {
                                    window.location.href = "/checkout";
                                }
                            });
                        }
                    });
                });
            });

            // $(document).ready(function () {
            //   $('#AddToCart').on('click', function (event) {
            //     event.preventDefault();
            //     const $form = $(this).closest('form');
            //     Shopify.queue = [];
            //     const variantID = $form.find('[name="id"]').val();
            //     let Qty = parseInt($('[name="quantity"]').val());
            //     Shopify.queue.push({
            //       variantId: variantID,
            //       quantity: Qty
            //     });
            //     $('[data-queue]').each(function () {
            //       if ($(this).find('input[type="checkbox"]').prop('checked') == true) {
            //         let variantID = $(this).find('input[type="checkbox"]').val();
            //         Qty = 1;
            //         let properties = {};
            //         properties["Addons"] = true;

            //         Shopify.queue.push({
            //           variantId: variantID,
            //           quantity: Qty,
            //           properties: properties
            //         });
            //       }
            //     })
            //     console.log(Shopify.queue)
            //     Shopify.moveAlong();
            //   })

            //   Shopify.moveAlong = function () {
            //     if (Shopify.queue.length) {
            //       var request = Shopify.queue.shift();
            //       Shopify.addItem(request.variantId, request.quantity, request.properties, Shopify.moveAlong);
            //     } else {
            //       //Shopify.AjaxifyCart.init();
            //       document.location.href = '/cart';
            //     }
            //   };

            //   Shopify.addItem = function (id, qty, properties, callback) {
            //     var params = {
            //       quantity: qty,
            //       id: id
            //     };
            //     if (properties != false) {
            //       params.properties = properties;
            //     }
            //     $.ajax({
            //       type: 'POST',
            //       url: '/cart/add.js',
            //       dataType: 'json',
            //       data: params,
            //       success: function () {
            //         if (typeof callback === 'function') {
            //           callback();
            //         }
            //       },
            //       error: function () {}
            //     });
            //   }
            // });

            $(".add-cart").click(function () {
                var addcart = $(this).data("attr");
                console.log(addcart);
                addtocart('#AddToCartForm-' + addcart);
            });

            function addtocart(addtocart) {
                jQuery.ajax({
                    type: "POST",
                    url: "/cart/add.js",
                    data: jQuery(addtocart).serialize(),
                    dataType: "json",
                    success: function (cart) {
                        CartJS.render(null, cart);
                        cartDrawer.offcanvas('open');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseJSON.description);
                    }
                });
            }

        };
        custom.init = () => {
            cacheDom();
            bindUIActions();
        };

    })(window.custom = window.custom || {}, $);
}