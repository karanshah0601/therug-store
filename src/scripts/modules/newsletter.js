// import $ from 'jquery';

export default () => {
    ((Newsletter, $) => {
        'use strict';

        const $dom = {};

        const cacheDom = () => {
            $dom.bodyElement = $('#mailchimp_form');
            $dom.formSuccess = $('.form_success');
            $dom.formError = $('.form_error');
        };

        const register = ($form) => {
            $.ajax({
                type: 'GET',
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: 'jsonp',
                cache: false,
                jsonp: 'c', // trigger MailChimp to return a JSONP response
                contentType: 'application/json; charset=utf-8',
                error: function (error) {
                    // According to jquery docs, this is never called for cross-domain JSONP requests
                    $form.hide();
                    $dom.formError.css('display', 'block');
                },
                success: function (data) {
                    if (data.result != "success") {
                        if (data.msg.indexOf('Please enter a value')) {
                            $dom.formError.css('display', 'block');
                        }
                        if (data.msg && data.msg.indexOf("already subscribed") >= 0) {
                            alert('You are already subscribed to the list.');
                        }
                    } else {
                        // hide the input area and show the success message
                        $form.hide();
                        $dom.formSuccess.css('display', 'block');
                    }
                }
            });
        };

        const bindUIActions = () => {

            $dom.bodyElement.find('input[type="submit"]').on('click', function (e) {
                e.preventDefault();
                register($dom.bodyElement);
            });
            $dom.bodyElement.submit(function (e) {
                e.preventDefault();
                register($dom.bodyElement);
            });

        };
        Newsletter.init = () => {
            cacheDom();
            bindUIActions();
        };

    })(window.Newsletter = window.Newsletter || {}, $);
}